<%--@elvariable id="post" type="com.example.forum.entity.Post"--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Edit Post</title>
</head>
<body>
<script src="<c:url value="/src/main/webapp/js/jquery-1.10.2.min.js" />" type="text/javascript"></script>

<div class="container">
    <form  class="form-signup" method="post" action="<c:url value="${pageContext.request.contextPath}/post/updateOrCreatePost"/>">
        <h2 class="form-signiup-heading">Edit Post</h2>
        <p>
            <label for="text">Post text</label>
            <input value="${post.text}" type="text" id="text" name="text" class="form-control" placeholder="text" required>

            <label for="postId"></label>
            <input value="${post.id==null ? "-1" : post.id}" id="postId" type="number" name="postId" class="form-control" placeholder="postId" required hidden>
        </p>

        <button class="btn btn-lg btn-primary btn-block" type="submit" >Save</button>
    </form>
    <form  class="form-signup" method="post" action="<c:url value="${pageContext.request.contextPath}/post/deletePost"/>">
        <label for="postId2"></label>
        <input value="${post.id}" id="postId2" type="number" name="postId" class="form-control" placeholder="postId" required hidden>
        <button class="btn btn-lg btn-primary btn-block" type="submit" >Delete</button>
    </form>
</div>
</body>
</html>
