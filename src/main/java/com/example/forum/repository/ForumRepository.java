package com.example.forum.repository;

import com.example.forum.entity.Comment;
import com.example.forum.entity.Post;
import com.example.forum.entity.User;

import java.util.List;

public interface ForumRepository {
    List<Post> getAllPosts();
    User getUserByName(String name);
    void addUser(User user);
    void addPost(String text, Integer postId, String username);
    void deletePost(Integer postId);
    Post getPostById(Integer postId);
    void addComment(String text, Integer commentId, Integer postId, String username);
    void deleteComment(Integer commentId);
    Comment getCommentById(Integer commentId);
    void editUser(String name, String role, Integer id);
    void addAutoComments();
}
