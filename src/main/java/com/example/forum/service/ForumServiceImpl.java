package com.example.forum.service;

import com.example.forum.entity.Comment;
import com.example.forum.entity.Post;
import com.example.forum.entity.User;
import com.example.forum.model.Role;
import com.example.forum.repository.ForumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;
@Component
public class ForumServiceImpl implements ForumService {
    @Autowired
    private ForumRepository repository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Override
    public List<Post> getAllPosts() {
        return repository.getAllPosts();
    }
    @Override
    public void addUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        repository.addUser(user);
    }
    @Override
    public void addPost(String text, Integer postId, String username) {
        repository.addPost(text, postId, username);
    }
    @Override
    public void deletePost(Integer postId) {
        repository.deletePost(postId);
    }
    @Override
    public Post getPostById(Integer postId){
        return repository.getPostById(postId);
    }

    @Override
    public Comment getCommentById(Integer commentId) {
        return repository.getCommentById(commentId);
    }

    @Override
    public void addComment(String text, Integer commentId, Integer postId, String username)
    {
        repository.addComment(text, commentId, postId, username);
    }

    @Override
    public void deleteComment(Integer commentId) {
        repository.deleteComment(commentId);
    }

    @Override
    public User getUserByName(String name) {
        return repository.getUserByName(name);
    }

    @Override
    public void editUser(String name, String role, Integer id) {
        repository.editUser(name, role, id);
    }

}
