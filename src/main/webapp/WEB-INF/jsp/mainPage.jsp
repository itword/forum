<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<jsp:directive.page contentType="text/html;charset=UTF-8"/>

<head>
    <link href="<c:url value="/css/style.css" />" rel="stylesheet" type="text/css">
    <title>Forum</title>
</head>
<body>
<script language="JavaScript" src="<c:url value="/js/jquery-1.10.2.min.js" />" type="text/javascript"></script>
<script>

</script>


<%--@elvariable id="posts" type="java.util.List<Post>"--%>
    <div id="page">
        <p><a href=${pageContext.request.contextPath}/post/addPost>Создать пост</a></p>
        <c:forEach var="post" items="${posts}">
            <div id="content" class="content">
                <c:if test="${role=='admin'}">
                <p><a href=${pageContext.request.contextPath}/post/addPost?id=${post.id}>Редактировать пост</a></p>
                </c:if>
                <a  <c:if test="${role=='admin'}">href="${pageContext.request.contextPath}/user/editUser?name=${post.getUser().getNickname()}"</c:if>>Автор поста ${post.getUser().getNickname()}</a>

                <div class="post_content">${post.getText()}</div>
                <div class="ufoot">
                    <div class="comment_list_post">
                        <c:forEach var="comment" items="${post.getComments()}">
                            <c:if test="${role=='admin'}">
                            <p><a href=${pageContext.request.contextPath}/comment/addComment?postId=${post.id}&id=${comment.id}>Редактировать коментарий</a></p>
                            </c:if>
                            <div class="comment">
                                <p>${comment.getText()}</p>
                                <div class="comments_bottom">Автор коментария ${comment.getUser().getNickname()}</div>
                            </div>
                        </c:forEach>
                        <p><a href=${pageContext.request.contextPath}/comment/addComment?postId=${post.id}>Создать коментарий</a></p>
                    </div>
                </div>

            </div>
        </c:forEach>
    </div>
</body>
</html>