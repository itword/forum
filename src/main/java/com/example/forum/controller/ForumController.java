package com.example.forum.controller;

import com.example.forum.entity.Comment;
import com.example.forum.entity.Post;
import com.example.forum.entity.User;
import com.example.forum.model.Role;
import com.example.forum.security.UserDetailsImpl;
import com.example.forum.service.ForumService;
import com.sun.javaws.jnl.RContentDesc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import javax.servlet.jsp.PageContext;

@RestController
public class ForumController {
    private final ForumService forumService;
    @Autowired
    public ForumController(ForumService forumService) {
        this.forumService = forumService;
    }

    @RequestMapping
    public ModelAndView mainPage(){
        ModelAndView model = new ModelAndView("mainPage");
        model.addObject("posts", forumService.getAllPosts());
        model.addObject("role",((UserDetailsImpl)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getAuthorities().iterator().next().toString());
        return model;
    }
    @RequestMapping("/auth/login")
    public ModelAndView getLoginPage(){
        ModelAndView model = new ModelAndView("login");
        return model;
    }
    @RequestMapping("/user/signUp")
    public ModelAndView getSignUPPage(){
        ModelAndView model = new ModelAndView("signUp");
        model.addObject("roles", Role.values());
        return model;

    }
    @RequestMapping(value ="/user/signUp", method = RequestMethod.POST)
    public void addUser( String nickname,String email, String password, String userRole, HttpServletResponse response) throws IOException {
        forumService.addUser(new User(nickname,email,password,userRole));
        response.sendRedirect(ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString());
    }
    @RequestMapping(value="/user/editUser",method = RequestMethod.GET)
    public ModelAndView editUser(String name)
    {
        ModelAndView model= new ModelAndView("user");

        model.addObject("user",forumService.getUserByName(name));
        model.addObject("roles", Role.values());

        return model;
    }
    @RequestMapping(value="/user/editUser",method = RequestMethod.POST)
    public void editUser(String userName, String userRole, Integer userId, HttpServletResponse response) throws IOException {
        UserDetailsImpl user = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if(user.getAuthorities().iterator().next().toString().equals("admin") || user.getUserId().equals(userId))
        {
            forumService.editUser(userName, userRole, userId);
        }
        response.sendRedirect(ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString());
    }

    @RequestMapping(value="/post/addPost",method = RequestMethod.GET)
    public ModelAndView addPost(Integer id)
    {
        ModelAndView model= new ModelAndView("post");
        if (id == null) {
            Post newPost = new Post();
            model.addObject("post",newPost);
        } else {
            model.addObject("post",forumService.getPostById(id));
        }
        return model;
    }

    @RequestMapping(value="/post/updateOrCreatePost",method = RequestMethod.POST)
    public void addPost(String text, Integer postId, HttpServletResponse response) throws IOException {
        if(postId==-1){
            postId = null;
        }
        forumService.addPost(text, postId, SecurityContextHolder.getContext().getAuthentication().getName());
        response.sendRedirect(ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString());
    }
    @RequestMapping(value="/post/deletePost",method = RequestMethod.POST)
    public void deletePost(Integer postId, HttpServletResponse response) throws IOException {
        forumService.deletePost(postId);
        response.sendRedirect(ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString());
    }

    @RequestMapping(value="/comment/addComment",method = RequestMethod.GET)
    public ModelAndView addComment(Integer postId, Integer id)
    {
        ModelAndView model= new ModelAndView("comment");
        if (id == null) {
            Comment newComment = new Comment();
            Post post=new Post();
            post.setId(postId);
            newComment.setPost(post);
            model.addObject("comment",newComment);
        } else {
            model.addObject("comment",forumService.getCommentById(id));
        }
        return model;
    }

    @RequestMapping(value="/comment/updateOrCreateComment",method = RequestMethod.POST)
    public void updateOrComment(String text, Integer commentId, Integer postId, HttpServletResponse response) throws IOException {
        if(commentId==-1){
            commentId=null;
        }
        forumService.addComment(text, commentId, postId, SecurityContextHolder.getContext().getAuthentication().getName());
        response.sendRedirect(ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString());
    }
    @RequestMapping(value="/comment/deleteComment",method = RequestMethod.POST)
    public void deleteComment(Integer commentId, HttpServletResponse response) throws IOException {
        forumService.deleteComment(commentId);
        response.sendRedirect(ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString());
    }

}
