<%--@elvariable id="user" type="com.example.forum.entity.User"--%>
<%--@elvariable id="roles" type="com.example.forum.model.Role.values()"--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Login Customer</title>
</head>
<body>
<script src="<c:url value="/js/jquery-1.10.2.min.js" />" type="text/javascript"></script>
<script>function setSelectedRole(){
    document.getElementById("userRole").value=document.getElementById("Role").value;
}</script>
<div class="container">
    <form class="form-signup" method="post" action="<c:url value="${pageContext.request.contextPath}/user/editUser"/>">
        <h2 class="form-signiup-heading">New Post</h2>
        <p>
            <label for="userName">User name</label>
            <input type="text" id="userName" name="userName" class="form-control" placeholder="userName" value="${user.nickname}" required>
            <select id="Role" onchange="setSelectedRole()">

                <c:forEach var="role" items="${roles}">
                    <option value="${role.name()}" >${role.name()}</option>
                </c:forEach>
            </select>
            <label for="userRole"></label>
            <input type="text" id="userRole" name="userRole" class="form-control" placeholder="userRole" value="admin" required hidden>
            <label for="userId"></label>
            <input type="number" id="userId" name="userId" class="form-control" placeholder="userId" value="${user.id}" required hidden>
        </p>

        <button class="btn btn-lg btn-primary btn-block" type="submit" >Save</button>
    </form>

</div>
</body>
</html>
