<%--@elvariable id="comment" type="com.example.forum.entity.Comment"--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Edit Comment</title>
</head>
<body>
<script src="<c:url value="/src/main/webapp/js/jquery-1.10.2.min.js" />" type="text/javascript"></script>

<div class="container">
    <form  class="form-signup" method="post" action="<c:url value="${pageContext.request.contextPath}/comment/updateOrCreateComment"/>">
        <h2 class="form-signiup-heading">Edit Comment</h2>
        <p>
            <label for="text">Post text</label>
            <input value="${comment.text}" type="text" id="text" name="text" class="form-control" placeholder="text" required>

            <label for="commentId"></label>
            <input value="${comment.id==null ? "-1" : comment.id}" id="commentId" type="number" name="commentId" class="form-control" placeholder="commentId" required hidden>
            <label for="postId"></label>
            <input value="${comment.post.id}" id="postId" type="number" name="postId" class="form-control" placeholder="postId" required hidden>
        </p>
        <button class="btn btn-lg btn-primary btn-block" type="submit" >Save</button>
    </form>
    <form  class="form-signup" method="post" action="<c:url value="${pageContext.request.contextPath}/comment/deleteComment"/>">
        <label for="commentId2"></label>
        <input value="${comment.id}" id="commentId2" type="number" name="commentId" class="form-control" placeholder="commentId" required hidden>
        <button class="btn btn-lg btn-primary btn-block" type="submit" >Delete</button>
    </form>
</div>
</body>
</html>
