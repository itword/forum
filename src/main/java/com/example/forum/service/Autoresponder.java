package com.example.forum.service;

import com.example.forum.repository.ForumRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class Autoresponder {
    private final ForumRepository repository;

    public Autoresponder(ForumRepository repository) {
        this.repository = repository;
    }

    @Scheduled(fixedDelay = 2000)
    public void addAutoComments(){
        repository.addAutoComments();
    }
}
