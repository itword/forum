<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Login Customer</title>
</head>
<body>
<script src="<c:url value="/js/jquery-1.10.2.min.js" />" type="text/javascript"></script>
<script>function setSelectedRole(){
    document.getElementById("userRole").value=document.getElementById("Role").value;
}</script>
<div class="container">
    <form class="form-signup" method="post" action="<c:url value="${pageContext.request.contextPath}/user/signUp"/>">
        <h2 class="form-signiup-heading">SignUp</h2>
        <p>
            <label for="username">Username</label>
            <input type="text" id="username" name="nickname" class="form-control" placeholder="Username" required>
        </p>
        <p>
            <label for="email">Username</label>
            <input type="text" id="email" name="email" class="form-control" placeholder="Email" required>
        </p>
        <p>
            <label for="password">Password</label>
            <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
        </p>
        <p>
            <select id="Role" onchange="setSelectedRole()">

                <c:forEach var="role" items="${roles}">
                    <option value="${role.name()}" >${role.name()}</option>
                </c:forEach>
            </select>
            <label for="userRole"></label>
            <input type="text" id="userRole" name="userRole" class="form-control" placeholder="userRole" value="admin" required hidden>
        </p>
        <button class="btn btn-lg btn-primary btn-block" type="submit" >Sign up</button>
    </form>
</div>
</body>
</html>
