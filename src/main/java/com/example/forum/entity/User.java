package com.example.forum.entity;

import com.example.forum.model.Role;
import com.example.forum.model.Status;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "user", schema = "forum")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "email")
    private String email;
    @Enumerated(value = EnumType.STRING)
    @Column(name = "role")
    private Role role;
    @Enumerated(value = EnumType.STRING)
    @Column(name = "status")
    private Status status;
    @Column(name = "nickname")
    private String nickname;
    @Column(name = "password")
    private String password;
    @OneToMany(mappedBy="user")
    private Collection<Post> posts;
    public User() {
    }
    public User(String nickname, String email, String password, String userRole) {
        this.nickname=nickname;
        this.email=email;
        this.password=password;
        this.status=Status.ACTIVE;
        this.role=Role.valueOf(userRole);
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Collection<Post> getPosts() {
        return posts;
    }

    public void setPosts(Collection<Post> posts) {
        this.posts = posts;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
