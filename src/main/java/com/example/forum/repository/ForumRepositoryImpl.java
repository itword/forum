package com.example.forum.repository;

import com.example.forum.entity.AutoComment;
import com.example.forum.entity.Comment;
import com.example.forum.entity.Post;
import com.example.forum.entity.User;
import com.example.forum.model.Role;
import com.example.forum.security.UserDetailsImpl;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.*;

@Repository
@Transactional
public class ForumRepositoryImpl implements ForumRepository {
    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public List<Post> getAllPosts() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Post> criteriaQuery = criteriaBuilder.createQuery(Post.class);
        Root<Post> postRoot = criteriaQuery.from(Post.class);
        criteriaQuery.select(postRoot);
        TypedQuery<Post> typedQuery = entityManager.createQuery(criteriaQuery);
        List<Post> postList = typedQuery.getResultList();
        return postList;
    }
    @Override
    public User getUserByName(String name){
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
        Root<User> userRoot = criteriaQuery.from(User.class);
        Predicate userPredicate = criteriaBuilder.equal(userRoot.get("nickname"),name);
        criteriaQuery.select(userRoot).where(userPredicate);
        TypedQuery<User> typedQuery = entityManager.createQuery(criteriaQuery);
        User user = typedQuery.getSingleResult();
        return user;
    }
    @Override
    public void addUser(User user){
        entityManager.persist(user);
    }
    @Override
    public void addPost(String text, Integer postId, String username){
        Post post;
        if(postId!=null) {
            post = getPostById(postId);
        }
        else {
            post=new Post();
            User user = getUserByName(username);
            post.setUser(user);
        }
        UserDetailsImpl user = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if(user.getAuthorities().iterator().next().toString().equals("admin") || user.getUserId().equals(post.getUser().getId()))
        post.setText(text);
        entityManager.persist(post);
        entityManager.flush();
    }
    @Override
    public void deletePost(Integer postId){
        Post post=entityManager.find(Post.class,postId);
        UserDetailsImpl user = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(user.getAuthorities().iterator().next().toString().equals("admin") || user.getUserId().equals(post.getUser().getId()))
        entityManager.remove(post);
    }
    @Override
    public Post getPostById(Integer postId){
        return entityManager.find(Post.class,postId);
    }

    @Override
    public void addComment(String text, Integer commentId, Integer postId, String username) {
        Comment comment;
        if(commentId!=null) {
            comment = getCommentById(commentId);
        }
        else {
            comment = new Comment();
            comment.setPost(getPostById(postId));
            comment.setUser(getUserByName(username));
        }
        UserDetailsImpl user = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if(user.getAuthorities().iterator().next().toString().equals("admin") || user.getUserId().equals(comment.getUser().getId()))
        comment.setText(text);

        entityManager.persist(comment);
    }

    @Override
    public void deleteComment(Integer commentId) {

        Comment comment=entityManager.find(Comment.class,commentId);
        UserDetailsImpl user = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(user.getAuthorities().iterator().next().toString().equals("admin") || user.getUserId().equals(comment.getUser().getId()))
            entityManager.remove(comment);
    }

    @Override
    public Comment getCommentById(Integer commentId) {
        return entityManager.find(Comment.class, commentId);
    }

    @Override
    public void editUser(String name, String role, Integer id) {
        User user =entityManager.find(User.class, id)  ;
        user.setNickname(name);
        user.setRole(Role.valueOf(role));
        entityManager.persist(user);
    }

    @Override
    public void addAutoComments() {
        List<Post> posts = getAllPosts();
        List<AutoComment> autoComments= getAutoComments(posts.size());
        for (int i=0;i<autoComments.size();i++){
            AutoComment autoComment=autoComments.get(i);
            autoComment.setIsUsed(true);
            Comment comment =new Comment(autoComments.get(i).getText());
            comment.setPost(posts.get(i));
            posts.get(i).getComments().add(comment);
            entityManager.persist(comment);
            entityManager.persist(autoComment);
            entityManager.persist(posts.get(i));
        }

    }
    public List<AutoComment> getAutoComments(Integer size) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<AutoComment> criteriaQuery = criteriaBuilder.createQuery(AutoComment.class);
        Root<AutoComment> postRoot = criteriaQuery.from(AutoComment.class);
        Predicate predicateForUnused
                = criteriaBuilder.equal(postRoot.get("isUsed"), false);
        criteriaQuery.where(predicateForUnused).select(postRoot);
        TypedQuery<AutoComment> typedQuery = entityManager.createQuery(criteriaQuery);
        return typedQuery.setMaxResults(size).getResultList();
    }
}
