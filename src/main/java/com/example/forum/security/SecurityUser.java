package com.example.forum.security;

import com.example.forum.entity.User;
import com.example.forum.model.Role;
import com.example.forum.model.Status;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.*;

public class SecurityUser implements UserDetails {

    private final String username;
    private final String password;
    private final List<SimpleGrantedAuthority> authorities;
    private final boolean isActive;

    public SecurityUser(String username, String password, List<SimpleGrantedAuthority> authorities, boolean isActive) {
        this.username = username;
        this.password = password;
        this.authorities = authorities;
        this.isActive = isActive;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return isActive;
    }

    @Override
    public boolean isAccountNonLocked() {
        return isActive;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return isActive;
    }

    @Override
    public boolean isEnabled() {
        return isActive;
    }

    public static UserDetails fromUser(User user){
        return new UserDetailsImpl(
                user.getPassword(),user.getNickname()
                , new HashSet<>(Collections.singletonList(user.getRole()))
                ,user.getStatus().equals(Status.ACTIVE)
                ,user.getStatus().equals(Status.ACTIVE)
                ,user.getStatus().equals(Status.ACTIVE)
                ,user.getStatus().equals(Status.ACTIVE)
                ,user.getId()
        );
    }

}
