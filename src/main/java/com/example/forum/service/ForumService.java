package com.example.forum.service;

import com.example.forum.entity.Comment;
import com.example.forum.entity.Post;
import com.example.forum.entity.User;

import java.util.List;

public interface ForumService {
    List<Post> getAllPosts();
    void addUser(User user);
    void addPost(String text, Integer postId, String username);
    void deletePost(Integer postId);
    Post getPostById(Integer postId);
    Comment getCommentById(Integer commentId);
    void addComment(String text, Integer commentId, Integer postId, String username);
    void deleteComment(Integer commentId);
    User getUserByName(String name);
    void editUser(String name, String role, Integer id);
}
